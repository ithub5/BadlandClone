using System.Collections.Generic;
using System.Linq;
using PlayerSystem.Utils;
using UnityEngine;

namespace Utils
{
    public static class MyExtensions
    {
        public static bool Contains(this LayerMask mask, int layer)
        {
            return mask == (mask | (1 << layer));
        }

        public static T Random<T>(this IEnumerable<T> collection)
        {
            int randomIndex = UnityEngine.Random.Range(0, collection.Count());
            return collection.ElementAt(randomIndex);
        }

        public static Side GetSide(this Vector2 vector)
        {
            Side result;
            if (Mathf.Abs(vector.x) > Mathf.Abs(vector.y))
            {
                if (vector.x > 0)
                {
                    result = Side.left;
                }
                else
                {
                    result = Side.right;
                }
            }
            else
            {
                if (vector.y > 0)
                {
                    result = Side.bottom;
                }
                else
                {
                    result = Side.top;
                }
            }

            return result;
        }
    }
}