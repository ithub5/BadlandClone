using CameraSystem;
using InputSystem;
using LevelSystem;
using LevelSystem.Data;
using Mono.Cecil.Cil;
using PlayerSystem;
using Services.SaveSystem;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core
{
    public class Game
    {
        private InputHandler _input;
        private GameObject _playerInstance;
        private PlayerInvoker _playerInvoker;
        
        private LevelMaker _levelMaker;
        private LevelsProgressInfo _levelsProgressInfo;

        private CameraSetUp _cameraSetUp;
        private GameObject _cameraTargetInstance;
        private CameraMover _cameraMover;

        private SaveLoad _saveLoad;
        
        public Game
            (
            InputHandler input, GameObject playerInstance, PlayerInvoker playerInvoker,
            LevelMaker levelMaker, LevelsProgressInfo levelsProgressInfo,
            CameraSetUp cameraSetUp, GameObject cameraTargetInstance, CameraMover cameraMover, 
            SaveLoad saveLoad
            )
        {
            _input = input;
            _playerInstance = playerInstance;
            _playerInvoker = playerInvoker;
            
            _levelMaker = levelMaker;
            _levelsProgressInfo = levelsProgressInfo;

            _cameraSetUp = cameraSetUp;
            _cameraTargetInstance = cameraTargetInstance;
            _cameraMover = cameraMover;

            _saveLoad = saveLoad;
            
            Start();
        }

        private void Start()
        {
            _input.Enable();
            _playerInvoker.Bind();
            
            _levelMaker.CreateLevel();
            _playerInstance.transform.position = _levelMaker.PlayerSpawnPosition.position;

            _cameraSetUp.transform.position = _levelMaker.CameraSpawnPosition.position;
            _cameraTargetInstance.transform.position = _levelMaker.CameraSpawnPosition.position;
            _cameraSetUp.SetTarget(_cameraTargetInstance.transform);
            _cameraMover.Start();
            
            Bind();
        }

        private void Bind()
        {
            _playerInvoker.OnPlayerDeath += PerformRestart;
            _playerInvoker.OnPlayerFinish += PerformFinish;
        }

        private void Expose()
        {
            _playerInvoker.OnPlayerDeath -= PerformRestart;
            _playerInvoker.OnPlayerFinish -= PerformFinish;
        }

        private void PerformRestart()
        {
            Expose();
            
            LevelsProgressInfo levelsProgressInfo = new LevelsProgressInfo(
                _levelsProgressInfo.LevelNumber, _levelMaker.LevelPartsIds);
            _saveLoad.Save(levelsProgressInfo);

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        private void PerformFinish()
        {
            Expose();
            
            LevelsProgressInfo levelsProgressInfo = new LevelsProgressInfo(_levelsProgressInfo.LevelNumber + 1);
            _saveLoad.Save(levelsProgressInfo);

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}