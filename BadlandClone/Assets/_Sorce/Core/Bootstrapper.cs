using System;
using CameraSystem;
using CameraSystem.Data;
using InputSystem;
using LevelSystem;
using LevelSystem.Data;
using PlayerSystem;
using PlayerSystem.Data;
using Services;
using Services.SaveSystem;
using Services.SaveSystem.Data;
using UnityEngine;

namespace Core
{
    public class Bootstrapper : MonoBehaviour
    {
        [Header("Player System")] 
        [SerializeField] private GameObject playerPrefab;

        [Header("Level System")] 
        [SerializeField] private LevelPartsSO levelPartsSO;

        [Header("Camera System")] 
        [SerializeField] private CameraSetUpDataSO cameraSetUpDataSO;

        [Header("Save System")] 
        [SerializeField] private SavePathsSO savePathsSO;
        
        private Game _game;

        private InputHandler _input;

        private GameObject _playerInstance;
        private PlayerInvoker _playerInvoker;
        private PlayerPhysics _playerPhysics;

        private LevelMaker _levelMaker;
        private LevelsProgressInfo _levelsProgressInfo;

        private GameObject _cameraSetUpInstance;
        private CameraSetUp _cameraSetUp;
        private GameObject _cameraTargetInstance;
        private CameraMover _cameraMover;

        private SaveLoad _saveLoad;

        private void Awake()
        {
            LoadData();
            
            _input = new InputHandler();
            
            InitPlayer();
            InitLevel();

            _game = new Game(_input, _playerInstance, _playerInvoker, _levelMaker, _levelsProgressInfo,
                _cameraSetUp, _cameraTargetInstance, _cameraMover, _saveLoad);
        }

        private void LoadData()
        {
            _saveLoad = new SaveLoad(savePathsSO);
            
            _saveLoad.Load(out _levelsProgressInfo);
        }

        private void InitPlayer()
        {
            _playerInstance = Instantiate(playerPrefab);
            _playerPhysics = _playerInstance.GetComponent<PlayerPhysics>();
            _playerInvoker = new PlayerInvoker(_input, _playerPhysics);
        }

        private void InitLevel()
        {
            _levelMaker = new LevelMaker(levelPartsSO, _levelsProgressInfo);

            _cameraSetUpInstance = Instantiate(cameraSetUpDataSO.CameraSetUpPrefab);
            _cameraSetUp = _cameraSetUpInstance.GetComponent<CameraSetUp>();
            _cameraTargetInstance = Instantiate(cameraSetUpDataSO.CameraTargetPrefab);
            _cameraMover = new CameraMover(_cameraTargetInstance.GetComponent<Rigidbody2D>(), cameraSetUpDataSO.CameraMoveSpeed);
        }
    }
}