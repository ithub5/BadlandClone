using UnityEngine;

namespace Services.SaveSystem.Data
{
    [CreateAssetMenu(fileName = "SavePaths", menuName = "SOs/Save/Paths")]
    public class SavePathsSO : ScriptableObject
    {
        [SerializeField] private string levelProgressInfoPath;
        
        public string LevelProgressInfoPath { get => $"{Application.persistentDataPath}/{levelProgressInfoPath}"; }
    }
}