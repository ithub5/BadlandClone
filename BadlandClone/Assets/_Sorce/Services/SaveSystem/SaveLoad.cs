using System.IO;
using System.Transactions;
using LevelSystem.Data;
using UnityEngine;
using Services.SaveSystem.Data;


namespace Services.SaveSystem
{
    public class SaveLoad
    {
        private SavePathsSO _savePaths;
        
        private SaveLoadJson _saveLoadJson; 
        
        public SaveLoad(SavePathsSO savePaths)
        {
            _savePaths = savePaths;
            _saveLoadJson = new SaveLoadJson();
        }

        public void Save(LevelsProgressInfo levelsProgressInfo)
        {
            _saveLoadJson.WriteToJson(levelsProgressInfo, _savePaths.LevelProgressInfoPath);
        }

        public void Load(out LevelsProgressInfo levelsProgressInfo)
        {
            levelsProgressInfo = File.Exists(_savePaths.LevelProgressInfoPath)
                ? _saveLoadJson.ReadFromJson<LevelsProgressInfo>(_savePaths.LevelProgressInfoPath)
                : new LevelsProgressInfo(1);
        }
    }
}