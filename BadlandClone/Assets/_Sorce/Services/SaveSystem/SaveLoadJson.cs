using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;

namespace Services.SaveSystem
{
    public class SaveLoadJson
    {
        public void WriteToJson(object objectToWrite, string fullPath)
        {
            string objectInJson = JsonConvert.SerializeObject(objectToWrite, Formatting.Indented);

            using (StreamWriter writer = new StreamWriter(fullPath))
            {
                writer.Write(objectInJson);
            }
        }

        public T ReadFromJson<T>(string fullPath)
        {
            string objectInJson;

            using (StreamReader reader = new StreamReader(fullPath))
            {
                objectInJson = reader.ReadToEnd();
            }

            return JsonConvert.DeserializeObject<T>(objectInJson);
        }
    }
}

