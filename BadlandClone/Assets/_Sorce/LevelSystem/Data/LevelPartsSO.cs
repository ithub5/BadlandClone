using UnityEditor;
using UnityEngine;

namespace LevelSystem.Data
{
    [CreateAssetMenu(fileName = "LevelParts", menuName = "SOs/Level/Parts")]
    public class LevelPartsSO : ScriptableObject
    {
        [field: SerializeField] public LevelPartData[] Parts { get; private set; }
        [field: SerializeField] public float OffObstaclesPercentage { get; private set; }
    }
}