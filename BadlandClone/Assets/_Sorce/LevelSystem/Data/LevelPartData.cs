using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelSystem.Data
{
    [System.Serializable]
    public class LevelPartData : MonoBehaviour
    {
        [field: SerializeField] [Tooltip("MUST BE UNIQUE")] public int PartId { get; private set; }
        [field: SerializeField] public GameObject PartPrefab { get; private set; }
        [field: SerializeField] public Transform PlayerSpawnPosition { get; private set; }
        [field: SerializeField] public Transform CameraSpawnPosition { get; private set; }
        [field: SerializeField] public Transform End { get; private set; }
        [field: SerializeField] public GameObject[] Obstacles { get; private set; }
        
        [field: SerializeField] public LevelPartData[] PossibleNeighbors { get; private set; }
        [field: SerializeField] public LevelPartData[] PossibleEndings { get; private set; }
    }
}
