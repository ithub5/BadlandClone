using Services;
using UnityEngine;

namespace LevelSystem.Data
{
    public class LevelsProgressInfo
    {
        public int LevelNumber { get; private set; }
        public int[] LevelPartsForRestart { get; private set; }

        public LevelsProgressInfo(int levelNumber, int[] levelPartsForRestart = null)
        {
            LevelNumber = levelNumber;
            LevelPartsForRestart = levelPartsForRestart;
        }
    }
}