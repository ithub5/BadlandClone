using LevelSystem.Data;
using UnityEngine;
using Utils;
using System.Linq;

namespace LevelSystem
{
    public class LevelMaker
    {
        private LevelPartsSO _partsSO;
        private LevelsProgressInfo _progressInfo;

        private LevelPartData[] _currentParts;
        
        public Transform PlayerSpawnPosition { get => _currentParts[0].PlayerSpawnPosition; } 
        public Transform CameraSpawnPosition { get => _currentParts[0].CameraSpawnPosition; } 

        public int[] LevelPartsIds { get => _currentParts.Select(levelPart => levelPart.PartId).ToArray(); } 

        public LevelMaker(LevelPartsSO partsSO, LevelsProgressInfo progressInfo)
        {
            _partsSO = partsSO;
            _progressInfo = progressInfo;
        }

        public void CreateLevel()
        {
            OrderParts();
            Construct();
        }

        private void OrderParts()
        {
            _currentParts = new LevelPartData[_progressInfo.LevelNumber + 1];
            
            if (_progressInfo.LevelPartsForRestart != null 
                && _progressInfo.LevelNumber == _progressInfo.LevelPartsForRestart.Length)
            {
                ChoosePartsFromSave();
            }
            else
            {
                ChoosePartsDynamically();
            }
        }

        private void ChoosePartsFromSave()
        {
            for (int i = 0; i < _currentParts.Length - 1; i++)
            {
                _currentParts[i] = _partsSO.Parts
                    .FirstOrDefault(part => part.PartId == _progressInfo.LevelPartsForRestart[i]);
            }

            _currentParts[^1] = _currentParts[^2].PossibleEndings
                .FirstOrDefault(part => part.PartId == _progressInfo.LevelPartsForRestart[^1]);
        }

        private void ChoosePartsDynamically()
        {
            _currentParts[0] = _partsSO.Parts.Random();
            
            for (int i = 1; i < _currentParts.Length - 1; i++)
            {
                _currentParts[i] = _currentParts[i - 1].PossibleNeighbors.Random();
            }

            _currentParts[^1] = _currentParts[^2].PossibleEndings.Random();
        }

        private void Construct()
        {
            Vector2 startPos = Vector2.zero;
            LevelPartData tempLevelPart;
            for (int i = 0; i < _currentParts.Length; i++)
            {
                tempLevelPart = Object.Instantiate(_currentParts[i].PartPrefab, startPos, new Quaternion())
                    .GetComponent<LevelPartData>();
                startPos = new Vector2(startPos.x + _currentParts[i].End.position.x, startPos.y + _currentParts[i].End.position.y);
                
                OffRandomObstacles(_partsSO.OffObstaclesPercentage, tempLevelPart);
            }
        }

        private void OffRandomObstacles(float percent, LevelPartData levelPart)
        {
            percent = Mathf.Clamp01(percent);
            
            int iterationsCount = (int)(levelPart.Obstacles.Length * percent);
            GameObject[] tempObstacles = levelPart.Obstacles.OrderBy(obstacle => Random.value).ToArray();
            for (int i = 0; i < iterationsCount; i++)
            {
                tempObstacles[i].SetActive(false);
            }
        }
    }
}
