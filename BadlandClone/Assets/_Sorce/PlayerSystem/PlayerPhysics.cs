using System;
using System.Collections.Generic;
using PlayerSystem.Data;
using PlayerSystem.Utils;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils;

namespace PlayerSystem
{
    public class PlayerPhysics : MonoBehaviour
    {
        [SerializeField] private PlayerDataSO settings;
        [SerializeField] private Rigidbody2D rb;

        private Vector2 _moveInput;
        private Vector2 _targetVelocity;
        private Vector2 _acceleration;

        private bool _isColliding;

        private Dictionary<Side, List<Collider2D>> _sidesColliding;
        private Side[] _allSides;

        public event Action OnPlayerDeath;
        public event Action OnPlayerFinish;

        private void Awake()
        {
            _sidesColliding = new Dictionary<Side, List<Collider2D>>();
            _allSides = new[] { Side.right, Side.left, Side.top, Side.bottom };
            
            _sidesColliding.Add(Side.right, new List<Collider2D>());
            _sidesColliding.Add(Side.left, new List<Collider2D>());
            _sidesColliding.Add(Side.top, new List<Collider2D>());
            _sidesColliding.Add(Side.bottom, new List<Collider2D>());
        }

        private void FixedUpdate()
        {
            if (_isColliding)
            {
                return;
            }
            
            if (_moveInput == Vector2.zero)
            {
                _acceleration = Vector2.zero;
                return;
            }
            
            Move();
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!settings.DeathLayers.Contains(other.gameObject.layer))
            {
                return;
            }
            
            OnPlayerDeath?.Invoke();
        }

        private void OnCollisionStay2D(Collision2D other)
        {
            CheckCollidingSides(other);
            if (_isColliding)
            {
                return;
            }
            
            _isColliding = true;
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            _isColliding = false;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (settings.ShrinkLayer.Contains(other.gameObject.layer))
            {
                ChangeScale(false);
                other.gameObject.SetActive(false);
            }
            else if (settings.EnlargeLayer.Contains(other.gameObject.layer))
            {
                ChangeScale(true);
                other.gameObject.SetActive(false);
            }
            else if (settings.FinishLayer.Contains(other.gameObject.layer))
            {
                OnPlayerFinish?.Invoke();
            }
        }

        public void UpdateInput(Vector2 movementInput)
        {
            _moveInput = movementInput;
        }
        
        private void Move()
        {
            _targetVelocity = (_isColliding ? Vector2.zero : _moveInput) * settings.MoveSpeed;
            if (CheckIfDontNeedMovement())
            {
                return;
            }
            
            Vector2 smoothedMovement = Vector2.SmoothDamp(
                rb.velocity, _targetVelocity, ref _acceleration, settings.MoveSmoothTime);
            rb.velocity = new Vector2(smoothedMovement.x, rb.velocity.y);
        }

        private bool CheckIfDontNeedMovement()
        {
            bool isGoingFasterThanMoving = Math.Abs(_targetVelocity.x) <= Math.Abs(rb.velocity.x);
            bool isGoingSameDirection = _moveInput.x == Mathf.Clamp(rb.velocity.x, -1, 1);
            
            return isGoingFasterThanMoving && isGoingSameDirection;
        }
        
        public void Jump()
        {
            rb.velocity *= Vector2.right;
            
            rb.AddForce(Vector2.up * settings.JumpForce, ForceMode2D.Impulse);
        }

        private void ChangeScale(bool enlargeOrShrink)
        {
            float scaleCoef = enlargeOrShrink 
                ? settings.EnlargeShrinkCoefficient
                : Mathf.Pow(settings.EnlargeShrinkCoefficient, -1f);

            transform.localScale *= scaleCoef;
            rb.mass *= scaleCoef;
        }

        private void CheckCollidingSides(Collision2D other)
        {
            Side collisionSide = other.contacts[0].normal.GetSide();
            if (_sidesColliding[collisionSide].Contains(other.collider))
            {
                return;
            }
            
            for (int i = 0; i < _allSides.Length; i++)
            {
                if (_allSides[i] == collisionSide || !_sidesColliding[_allSides[i]].Contains(other.collider))
                {
                    continue;
                }

                _sidesColliding[_allSides[i]].Remove(other.collider);
            }
            
            CheckIfSqueezed();
        }

        private void CheckIfSqueezed()
        {
            if ((_sidesColliding[Side.right].Count > 0 && _sidesColliding[Side.left].Count > 0)
                || (_sidesColliding[Side.top].Count > 0 && _sidesColliding[Side.bottom].Count > 0))
            {
                OnPlayerDeath?.Invoke();
            }
        }
    }
}