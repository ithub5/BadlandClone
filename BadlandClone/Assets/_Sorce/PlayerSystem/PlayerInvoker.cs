using System;
using InputSystem;
using PlayerSystem.Data;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PlayerSystem
{
    public class PlayerInvoker
    {
        private InputHandler _input;
        
        private PlayerPhysics _physics;

        public PlayerInvoker(InputHandler input, PlayerPhysics physics)
        {
            _input = input;
            _physics = physics;
        }

        public event Action OnPlayerDeath;
        public event Action OnPlayerFinish;
        
        public void Bind()
        {
            _input.Basic.Movemet.performed += PerformMove;
            _input.Basic.Movemet.canceled += PerformMove;
            _input.Basic.Jump.performed += PerformJump;

            _physics.OnPlayerDeath += PerformDeath;
            _physics.OnPlayerFinish += PerformFinish;
        }

        public void Expose()
        {
            _input.Basic.Movemet.performed -= PerformMove;
            _input.Basic.Movemet.canceled -= PerformMove;
            _input.Basic.Jump.performed -= PerformJump;

            _physics.OnPlayerDeath -= PerformDeath;
            _physics.OnPlayerFinish -= PerformFinish;
        }

        private void PerformMove(InputAction.CallbackContext ctx)
        {
            _physics.UpdateInput(ctx.ReadValue<Vector2>());
        }
        
        private void PerformJump(InputAction.CallbackContext ctx)
        {
            _physics.Jump();
        }

        private void PerformDeath()
        {
            Expose();
            
            OnPlayerDeath?.Invoke();
        }

        private void PerformFinish()
        {
            Expose();
            
            OnPlayerFinish?.Invoke();
        }
    }
}