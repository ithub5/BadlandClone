namespace PlayerSystem.Utils
{
    public enum Side
    {
        right = 0,
        left = 1,
        top = 2,
        bottom = 3
    }
}