using UnityEngine;

namespace PlayerSystem.Data
{
    [CreateAssetMenu(fileName = "PlayerPhysicsData", menuName = "SOs/Player/PhysicsData")]
    public class PlayerDataSO : ScriptableObject
    {
        [field: SerializeField] public float MoveSpeed { get; private set; }
        [field: SerializeField] public float MoveSmoothTime { get; private set; }
        [field: SerializeField] public float JumpForce { get; private set; }
        [field: SerializeField] public LayerMask DeathLayers { get; private set; }
        [field: SerializeField] public float EnlargeShrinkCoefficient { get; private set; }
        [field: SerializeField] public LayerMask ShrinkLayer { get; private set; }
        [field: SerializeField] public LayerMask EnlargeLayer { get; private set; }
        [field: SerializeField] public LayerMask FinishLayer { get; private set; }
        
    }
}