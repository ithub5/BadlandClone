using Cinemachine;
using UnityEngine;

namespace CameraSystem
{
    public class CameraSetUp : MonoBehaviour
    {
        [field: SerializeField] public Camera Camera { get; private set; }
        [field: SerializeField] public CinemachineVirtualCamera VirtualCamera { get; private set; }

        public void SetTarget(Transform target)
        {
            VirtualCamera.Follow = target;
        }
    }
}