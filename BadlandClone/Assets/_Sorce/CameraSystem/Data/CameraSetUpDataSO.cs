using UnityEngine;

namespace CameraSystem.Data
{
    [CreateAssetMenu(fileName = "CameraSetUp", menuName = "SOs/Camera/SetUpData")]
    public class CameraSetUpDataSO : ScriptableObject
    {
        [field: SerializeField] public GameObject CameraSetUpPrefab { get; private set; }
        [field: SerializeField] public GameObject CameraTargetPrefab { get; private set; }
        [field: SerializeField] public float CameraMoveSpeed { get; private set; }
    }
}