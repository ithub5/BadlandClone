using UnityEngine;

namespace CameraSystem
{
    public class CameraMover
    {
        private Rigidbody2D _rb;
        private float _velocity;
        
        public CameraMover(Rigidbody2D rb, float velocity)
        {
            _rb = rb;
            _velocity = velocity;
        }

        public void Start()
        {
            _rb.velocity = _velocity * Vector2.right;
        }
    }
}