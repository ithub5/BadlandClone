namespace InputSystem
{
    public class InputHandler
    {
        private MainAction _action;
        
        public MainAction.BasicActions Basic { get => _action.Basic; }

        public InputHandler()
        {
            _action = new MainAction();
        }

        public void Enable()
        {
            _action.Enable();
            
        }

        public void Disable()
        {
            _action.Disable();
        }
    }
}
